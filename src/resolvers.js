
import mongodb from '/path/to/mongodb' // к MongoDB.

const  resolvers= {
  Query: {
    flavors: (parent, args) => {
      //  { id: '1' }
      return mongodb.collection('flavors').find(args).toArray()
    },
  },
  Mutation: {
    updateFlavor: (parent, args) => {
      //{ id: '1', name: 'Movie Theater Clone', description: 'Bring the movie theater taste home!' }

      mongodb.collection('flavors').update(args)
      return mongodb.collection('flavors').findOne(args.id)
    },
  },
  Flavor: {
    nutrition: (parent) => {
      return mongodb.collection('nutrition').findOne({
        flavorId: parent.id,
      })
    }
  },
};

export default resolvers;